import express from "express";
import { Request, Response } from "express";
import log from "@ajar/marker";
import { router } from "./routerFunctions.js";

const { PORT = 3030, HOST = "localhost" } = process.env;

const app = express();
app.use(express.json());

app.get("/", (req: Request, res: Response) => {
  res.status(200).send("Hello Express!");
});

app.use("/", router);

app.use("*", (req: Request, res: Response) => {
  res
    .status(404)
    .set("Content-Type", "text/html")
    .send(`<h1>${req.originalUrl} is unsupported route</h1>`);
});

app.listen(Number(PORT), HOST, () => {
  log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
});
