import { Request, Response, NextFunction, Router } from "express";
import morgan from "morgan";

const router = Router();

router.use(morgan("dev"));

const middleware = (req: Request, res: Response, next: NextFunction) => {
  console.log("I am a middleare!");
  next();
};

router.get("/users/:userID", middleware, (req: Request, res: Response) => {
  res.status(200).json(req.params);
});

router.get("/search", (req: Request, res: Response) => {
  res.status(200).json(req.query);
});

router.post("/users/addUser", (req: Request, res: Response) => {
  res.status(200).json(req.body);
});

router.get("/users", (req: Request, res: Response) => {
  res.status(200).send("Get all Users");
});

export { router };
